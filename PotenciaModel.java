
public class PotenciaModel {
	//private double amplitudeTensao;
	//private double anguloFaseTensao;
	//private double amplitudeCorrente;
	//private double anguloFaseCorrente;
	private double pAtiva;
	private double pReativa;
	private double pAparente;
	private double fPotencia;
	
	
	public void calcularPotenciaAtiva(double amplitudeTensao, double amplitudeCorrente, double anguloFaseTensao, double anguloFaseCorrente) {
		pAtiva = amplitudeTensao * amplitudeCorrente * (Math.cos(Math.toRadians(anguloFaseTensao - anguloFaseCorrente)));
	}
	public double getPotenciaAtiva() {
		return pAtiva;
	}
	public void calcularPotenciaReativa(double amplitudeTensao, double amplitudeCorrente, double anguloFaseTensao, double anguloFaseCorrente) {
		pReativa = amplitudeTensao * amplitudeCorrente * (Math.sin(Math.toRadians(anguloFaseTensao - anguloFaseCorrente)));
	}
	public double getPotenciaReativa() {
		return pReativa;
	}
	public void calcularPotenciaAparente(double amplitudeTensao, double amplitudeCorrente, double anguloFaseTensao, double anguloFaseCorrente) {
		pAparente = amplitudeTensao * amplitudeCorrente;
	}
	public double getPotenciaAparente() {
		return pAparente;
	}
	public void calcularFatorPotencia(double amplitudeTensao, double amplitudeCorrente, double anguloFaseTensao, double anguloFaseCorrente) {
		fPotencia = Math.cos(Math.toRadians(anguloFaseTensao - anguloFaseCorrente));
	}
	public double getFatorPotencia() {
		return fPotencia;
	}
}
