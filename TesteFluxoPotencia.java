import static org.junit.Assert.*;

import org.junit.Test;

public class TesteFluxoPotencia {

	@Test
	public void testarPotenciaAtiva() {
		PotenciaModel teste = new PotenciaModel();
		System.out.println("Potência Ativa OK");
		//PotenciaView teste = new PotenciaView();
		double resultadoEsperado = 0.0;
		double resultado = teste.getPotenciaAtiva();
		assertEquals(resultadoEsperado, resultado, 0.0);
	}
	@Test
	public void testarPotenciaReativa() {
		System.out.println("Potência Reativa OK");
		PotenciaModel teste = new PotenciaModel();
		double resultadoEsperado = 0.0;
		double resultado = teste.getPotenciaReativa();
		assertEquals(resultadoEsperado, resultado, 0.0);
	}
	@Test
	public void testarPotenciaAparente() {
		System.out.println("Potência Aparente OK");
		PotenciaModel teste = new PotenciaModel();
		double resultadoEsperado = 0.0;
		double resultado = teste.getPotenciaAparente();
		assertEquals(resultadoEsperado, resultado, 0.0);
	}
	@Test
	public void testarFatorPotencia() {
		System.out.println("Fator de Potência OK");
		PotenciaModel teste = new PotenciaModel();
		double resultadoEsperado = 0.0;
		double resultado = teste.getFatorPotencia();
		assertEquals(resultadoEsperado, resultado, 0.0);
	}

}
