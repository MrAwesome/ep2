
public class PotenciaMain {

	public static void main(String[] args) {
		PotenciaView view = new PotenciaView();
		PotenciaModel model =  new PotenciaModel();
		PotenciaControll controll = new PotenciaControll(view, model);
		
		view.setVisible(true);

	}

}
