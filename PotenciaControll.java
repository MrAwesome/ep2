import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class PotenciaControll {
	private PotenciaView view;
	private PotenciaModel model;
	
	public PotenciaControll(PotenciaView view, PotenciaModel model) {
		this.view = view;
		this.model = model;
		
		this.view.addCalculateListener(new CalculateListener());
	}
	class CalculateListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			double ampTensao, angFaseTensao, ampCorrente, angFaseCorrente;
			
			try {
				if(view.getAmplitudeTensao() > 220) {
					JOptionPane.showMessageDialog(null,"Amplitude de tensão entre 0 e 220");					
				}
				if(view.getAmplitudeCorrente() < 0 || view.getAmplitudeCorrente() > 100) {
					JOptionPane.showMessageDialog(null, "Amplitude da corrente entre 0 e 100");
				}
				if(view.getAnguloFaseTensao() < -180 || view.getAnguloFaseTensao() > 180) {
					JOptionPane.showMessageDialog(null, "Angulo de Fase entre -180º e 180º");
				}
				if(view.getAnguloFaseCorrente() < -180 || view.getAnguloFaseCorrente() > 180) {
					JOptionPane.showMessageDialog(null, "Angulo de Fase entre -180º e 180º");
				}
				ampTensao = view.getAmplitudeTensao();
				angFaseTensao = view.getAnguloFaseTensao();
				ampCorrente = view.getAmplitudeCorrente();
				angFaseCorrente = view.getAnguloFaseCorrente();
				
				model.calcularPotenciaAtiva(ampTensao, ampCorrente, angFaseTensao, angFaseCorrente);
				model.calcularPotenciaReativa(ampTensao, ampCorrente, angFaseTensao, angFaseCorrente);
				model.calcularPotenciaAparente(ampTensao, ampCorrente, angFaseTensao, angFaseCorrente);
				model.calcularFatorPotencia(ampTensao, ampCorrente, angFaseTensao, angFaseCorrente);
				
				view.setCalcularPotenciaAtiva(model.getPotenciaAtiva());
				view.setCalcularPotenciaReativa(model.getPotenciaReativa());
				view.setCalcularPotenciaAparente(model.getPotenciaAparente());
				view.setCalcularFatorPotencia(model.getFatorPotencia());
				
			}catch(NumberFormatException ex) {
				System.out.println(ex);
				view.displayErrorMessage("Insira número válido");
			}
		}
	}

}
