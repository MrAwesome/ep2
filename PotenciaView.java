import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Font;

public class PotenciaView extends JFrame{
	private JLabel ampT = new JLabel("Veff");
	private JLabel ampC = new JLabel("Aeff");
	private JLabel angFT = new JLabel("Ângulo de fase");
	private JLabel angFC = new JLabel("Ângulo de fase");
	private JTextField amplitudeTensao = new JTextField(10);
	private JTextField anguloFaseTensao = new JTextField(10);
	private JTextField amplitudeCorrente = new JTextField(10);
	private JTextField anguloFaseCorrente = new JTextField(10);
	private JTextField pAtiva = new JTextField(10);
	private JButton calculateButton = new JButton("OK");
	private final JLabel lblPotnciaReativa = new JLabel("Potência Reativa");
	private JTextField pReativa;
	private JTextField pAparente;
	private JTextField fPotencia;
	
	PotenciaView(){
		JPanel calcPanel = new JPanel();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1039, 774);
		
		calcPanel.setBounds(100, 100, 1066, 841);
		amplitudeTensao.setBounds(53, 90, 114, 19);
		anguloFaseTensao.setBounds(53, 148, 114, 19);
		amplitudeCorrente.setBounds(53, 216, 114, 19);
		anguloFaseCorrente.setBounds(53, 274, 114, 19);
		calculateButton.setBounds(962, 708, 54, 25);
		calcPanel.setLayout(null);
		ampT.setBounds(53, 59, 41, 19);
		
		calcPanel.add(ampT);
		calcPanel.add(amplitudeTensao);
		angFT.setBounds(53, 121, 106, 15);
		calcPanel.add(angFT);
		calcPanel.add(anguloFaseTensao);
		ampC.setBounds(53, 189, 28, 15);
		calcPanel.add(ampC);
		calcPanel.add(amplitudeCorrente);
		angFC.setBounds(53, 247, 106, 15);
		calcPanel.add(angFC);
		calcPanel.add(anguloFaseCorrente);
		calcPanel.add(calculateButton);
		pAtiva.setEditable(false);
		calcPanel.add(pAtiva);
		
		amplitudeTensao.setColumns(10);
		anguloFaseTensao.setColumns(10);
		amplitudeCorrente.setColumns(10);
		anguloFaseCorrente.setColumns(10);
		pAtiva.setHorizontalAlignment(SwingConstants.CENTER);
		pAtiva.setBounds(53, 515, 114, 19);
		pAtiva.setColumns(10);
		
		
		getContentPane().add(calcPanel, BorderLayout.CENTER);
		
		JLabel lblPotnciaAtiva = new JLabel("Potência Ativa");
		lblPotnciaAtiva.setBounds(53, 493, 114, 25);
		calcPanel.add(lblPotnciaAtiva);
		
		JLabel lblEntradas = new JLabel("ENTRADAS");
		lblEntradas.setBounds(351, 12, 105, 32);
		calcPanel.add(lblEntradas);
		
		JLabel lblSaidas = new JLabel("SAIDAS");
		lblSaidas.setBounds(351, 343, 70, 15);
		calcPanel.add(lblSaidas);
		lblPotnciaReativa.setBounds(53, 546, 127, 15);
		
		calcPanel.add(lblPotnciaReativa);
		
		pReativa = new JTextField();
		pReativa.setEditable(false);
		pReativa.setBounds(53, 573, 114, 19);
		calcPanel.add(pReativa);
		pReativa.setColumns(10);
		
		JLabel lblPotnciaAparente = new JLabel("Potência Aparente");
		lblPotnciaAparente.setBounds(53, 604, 164, 15);
		calcPanel.add(lblPotnciaAparente);
		
		pAparente = new JTextField();
		pAparente.setEditable(false);
		pAparente.setBounds(53, 631, 114, 19);
		calcPanel.add(pAparente);
		pAparente.setColumns(10);
		
		JLabel lblFatorDePotncia = new JLabel("Fator de Potência");
		lblFatorDePotncia.setBounds(53, 662, 140, 15);
		calcPanel.add(lblFatorDePotncia);
		
		fPotencia = new JTextField();
		fPotencia.setEditable(false);
		fPotencia.setBounds(53, 689, 114, 19);
		calcPanel.add(fPotencia);
		fPotencia.setColumns(10);
	}
	public double getAmplitudeTensao() {
			return Double.parseDouble(amplitudeTensao.getText());
	}
	public double getAnguloFaseTensao() {
		return Double.parseDouble(anguloFaseTensao.getText());
	}
	public double getAmplitudeCorrente() {
		return Double.parseDouble(amplitudeCorrente.getText());
	}
	public double getAnguloFaseCorrente() {
		return Double.parseDouble(amplitudeCorrente.getText());
	}
	public double getCalcularPotenciaAtiva() {
		return Double.parseDouble(pAtiva.getText());
	}
	public double getCalcularPotenciaReativa() {
		return Double.parseDouble(pReativa.getText());
	}
	public double getCalcularPotenciaAparente() {
		return Double.parseDouble(pAparente.getText());
	}
	public double getCalcularFatorPotencia() {
		return Double.parseDouble(fPotencia.getText());
	}
	
	public void setCalcularPotenciaAtiva(double potenciaAtiva) {
		pAtiva.setText(Double.toString(potenciaAtiva));
	}
	
	public void setCalcularPotenciaReativa(double potenciaReativa) {
		pReativa.setText(Double.toString(potenciaReativa));
	}
	public void setCalcularPotenciaAparente(double potenciaAparente) {
		pAparente.setText(Double.toString(potenciaAparente));
	}
	public void setCalcularFatorPotencia(double fatorPotencia) {
		fPotencia.setText(Double.toString(fatorPotencia));
	}
	
	
	void addCalculateListener(ActionListener listenForOKButton) {
		calculateButton.addActionListener(listenForOKButton);
	}
	void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}	
}
